FROM adoptopenjdk/openjdk11:alpine-jre
EXPOSE 8080
ARG JAR_FILE=target/casco.jar
ADD ${JAR_FILE} app.jar
ENV JAVA_OPTS="-Xms1g -Xmx2g"
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar /app.jar"]
