package com.swedbank.casco.components;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.swedbank.casco.exceptions.VehicleProducerNotFoundException;
import com.swedbank.casco.models.Setting;
import com.swedbank.casco.models.Vehicle;
import com.swedbank.casco.models.VehicleProducer;
import com.swedbank.casco.services.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.Optional;

import static com.swedbank.casco.models.SettingType.RISK_COEFFICIENT;
import static com.swedbank.casco.utils.Constants.Data.DEFAULT_MAKE_COEFFICIENT;
import static com.swedbank.casco.utils.Constants.Message.RECORD_IMPORT_SKIPPED;

/**
 * Component to initialize data on startup
 *
 * @author Vinod John
 */
@Component
@Slf4j
public class DataInit {
    @Autowired
    ResourceLoader resourceLoader;

    @Autowired
    private SettingService settingService;

    @Autowired
    private VehicleProducerService vehicleProducerService;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private InsuranceFeeService insuranceFeeService;

    @Autowired
    private ValidationService validationService;

    @PostConstruct
    public void init() {
        initSettings();
        initVehicleData();
    }

    // PRIVATE METHODS //
    private void initSettings() {
        try {
            log.info("Starting Settings data import..");
            Reader reader = new InputStreamReader(resourceLoader.getResource("classpath:data.json").getInputStream());
            JsonObject mainData = JsonParser.parseReader(reader).getAsJsonObject().getAsJsonObject("data");
            JsonObject coefficientObject = mainData.getAsJsonObject("coefficients");
            JsonObject makeCoefficientObject = mainData.getAsJsonObject("make_coefficients");
            JsonObject avgPurchasePriceObject = mainData.getAsJsonObject("avg_purchase_price");

            coefficientObject.keySet().forEach(key -> {
                Setting setting = new Setting();
                setting.setSettingType(RISK_COEFFICIENT);
                setting.setName(key);
                setting.setValue(coefficientObject.get(key).getAsString());
                setting.setActive(true);
                settingService.createSetting(setting);
            });

            avgPurchasePriceObject.keySet().forEach(key -> {
                VehicleProducer vehicleProducer = new VehicleProducer();
                vehicleProducer.setName(key);
                vehicleProducer.setAveragePurchasePrice(avgPurchasePriceObject.get(key).getAsBigDecimal());
                vehicleProducer.setRiskCoefficient(DEFAULT_MAKE_COEFFICIENT);
                vehicleProducerService.createVehicleProducer(vehicleProducer);
            });

            makeCoefficientObject.keySet().forEach(key -> {
                String name = key.trim().replace(" ", "_").toUpperCase();
                Optional<VehicleProducer> vehicleProducerOptional = vehicleProducerService
                        .findVehicleProducerByName(name);

                if (vehicleProducerOptional.isEmpty()) {
                    VehicleProducer vehicleProducer = new VehicleProducer();
                    vehicleProducer.setName(name);
                    vehicleProducer.setAveragePurchasePrice(BigDecimal.ZERO);
                    vehicleProducer.setRiskCoefficient(makeCoefficientObject.get(key).getAsDouble());
                    vehicleProducerService.createVehicleProducer(vehicleProducer);
                } else {
                    VehicleProducer vehicleProducer = vehicleProducerOptional.get();
                    vehicleProducer.setRiskCoefficient(makeCoefficientObject.get(key).getAsDouble());
                    try {
                        vehicleProducerService.updateVehicleProducer(vehicleProducer);
                    } catch (VehicleProducerNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });

            log.info("Settings data imported to database successfully.");
        } catch (IOException e) {
            log.error("Error importing settings data file!");
            e.printStackTrace();
        }
    }

    private void initVehicleData() {
        try {
            log.info("Starting Vehicles data import and its insurance fee calculations..");
            InputStreamReader reader = new InputStreamReader(resourceLoader.getResource("classpath:vehicles.csv")
                    .getInputStream());
            CSVParser records = new CSVParser(reader, CSVFormat.DEFAULT.withDelimiter(',').withFirstRecordAsHeader()
                    .withIgnoreEmptyLines(true));
            int counter = 0, successCounter = 0;

            for (CSVRecord record : records) {
                counter++;

                try {
                    int indexIncrementer = record.size() - records.getHeaderMap().size();

                    if (1 == indexIncrementer) {
                        Vehicle vehicle = new Vehicle();

                        String plateNumber = formatPlateNumber(record.get(records.getHeaderMap()
                                .get("plate_number") + indexIncrementer));
                        validationService.validatePlateNumber(plateNumber);
                        vehicle.setPlateNumber(plateNumber);

                        int regYear = Integer.parseInt(numberFormattedString(record.get(records.getHeaderMap()
                                .get("first_registration") + indexIncrementer)));
                        validationService.validateRegistrationYear(regYear);
                        vehicle.setFirstRegistrationYear(regYear);

                        String vehicleProducerName = formatNumberToString(record.get(records.getHeaderMap()
                                .get("producer") + indexIncrementer));
                        VehicleProducer vehicleProducer = validationService.validateVehicleProducer(vehicleProducerName);
                        vehicle.setVehicleProducer(vehicleProducer);

                        Long mileage = Long.valueOf(numberFormattedString(record.get(records.getHeaderMap()
                                .get("mileage") + indexIncrementer)));
                        validationService.validateMileage(mileage);
                        vehicle.setMileage(mileage);

                        BigDecimal purchasePrice = BigDecimal.valueOf(Double.parseDouble(numberFormattedString(record
                                .get(records.getHeaderMap().get("purchase_price") + indexIncrementer))));
                        validationService.validatePurchasePrice(purchasePrice);
                        vehicle.setPurchasePrice(purchasePrice);

                        BigDecimal previousIndemnity = BigDecimal.valueOf(Double.parseDouble(numberFormattedString(record
                                .get(records.getHeaderMap().get("previous_indemnity") + indexIncrementer))));
                        validationService.validatePreviousIndemnity(previousIndemnity);
                        vehicle.setPreviousIndemnity(previousIndemnity);

                        vehicleService.createVehicle(vehicle);
                        insuranceFeeService.calculateFee(vehicle, true);
                        successCounter++;
                    } else {
                        throw new ArrayIndexOutOfBoundsException();
                    }
                } catch (NumberFormatException e) {
                    log.warn(RECORD_IMPORT_SKIPPED + (record.getRecordNumber() - 1) +
                            ". Reason: Invalid number found in record!");
                } catch (ArrayIndexOutOfBoundsException e) {
                    log.warn(RECORD_IMPORT_SKIPPED + (record.getRecordNumber() - 1) +
                            ". Reason: Some data missing/wrong format in record!");
                } catch (Exception e) {
                    log.warn(RECORD_IMPORT_SKIPPED + (record.getRecordNumber() - 1) +
                            ". Reason: " + e.getLocalizedMessage());
                }
            }

            log.info("Vehicles data (" + successCounter + "/" + counter + ") imported to database successfully.");
        } catch (IOException e) {
            log.error("Error importing vehicles data file!");
            e.printStackTrace();
        }
    }

    // Hacks to avoid number format exceptions
    private String formatNumberToString(String input) {
        return input.trim().toUpperCase().replace("0", "O");
    }

    private String formatStringToNumber(String input) {
        return input.trim().toUpperCase().replace("O", "0");
    }

    private String numberFormattedString(String numberedString) {
        return numberedString.isBlank() ? "0" : formatStringToNumber(numberedString)
                .replaceAll("\\.{2,}", ".");
    }

    private String formatPlateNumber(String vehicleProducerName) {
        if (6 != vehicleProducerName.length()) {
            return vehicleProducerName;
        } else {
            String formattedNumber = formatStringToNumber(vehicleProducerName.substring(0, 3));
            String formattedString = formatNumberToString(vehicleProducerName.substring(3, 6));
            return formattedNumber.concat(formattedString);
        }
    }
}
