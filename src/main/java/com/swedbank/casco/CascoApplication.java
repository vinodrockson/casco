package com.swedbank.casco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CascoApplication {
    public static void main(String[] args) {
        SpringApplication.run(CascoApplication.class, args);
    }
}
