package com.swedbank.casco.dtos;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

/**
 * Vehicle Producer Dto
 *
 * @author Vinod John
 */
@Data
@Builder
public class VehicleProducerDto {
    private UUID id;
    private String name;
}
