package com.swedbank.casco.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Vehicle List Dto
 *
 * @author Vinod John
 */
@Getter
@AllArgsConstructor
public class VehicleListDto {
    private final List<VehicleDto> vehicleList;
    private final int currentPage;
    private final long totalElements;
}
