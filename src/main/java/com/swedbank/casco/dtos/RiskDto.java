package com.swedbank.casco.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

/**
 * Risk Dto
 *
 * @author Vinod John
 */
@Getter
@AllArgsConstructor
public class RiskDto {
    private final UUID id;
    private final String name;
    private final boolean isCountedForCalculation;
}
