package com.swedbank.casco.dtos;

import com.swedbank.casco.models.VehicleProducer;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Vehicle Producer List Dto
 *
 * @author Vinod John
 */
@Getter
@AllArgsConstructor
public class VehicleProducerListDto {
    private final List<VehicleProducer> vehicleProducerList;
    private final int currentPage;
    private final long totalElements;
}
