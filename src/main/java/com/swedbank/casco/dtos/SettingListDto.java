package com.swedbank.casco.dtos;

import com.swedbank.casco.models.Setting;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Setting List Dto
 *
 * @author Vinod John
 */
@Getter
@AllArgsConstructor
public class SettingListDto {
    private final List<Setting> settingList;
    private final int currentPage;
    private final long totalElements;
}
