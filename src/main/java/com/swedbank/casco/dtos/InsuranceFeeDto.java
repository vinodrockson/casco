package com.swedbank.casco.dtos;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Insurance Fee Dto
 *
 * @author Vinod John
 */
@Getter
@Builder
public class InsuranceFeeDto {
    private final UUID id;
    private final BigDecimal annualFee;
    private final BigDecimal monthlyFee;
    private final String vehicleProducer;
    private final int firstRegistrationYear;
    private final Long mileage;
    private final BigDecimal previousIndemnity;
    private final LocalDateTime feeCalculatedDate;
    private final List<RiskDto> risks;
}
