package com.swedbank.casco.dtos;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Vehicle Dto
 *
 * @author Vinod John
 */
@Getter
@Builder
public class VehicleDto {
    private final UUID id;
    private final String plateNumber;
    private final int firstRegistrationYear;
    private final BigDecimal purchasePrice;
    private final String vehicleProducer;
    private final Long mileage;
    private final BigDecimal previousIndemnity;
    private final String createdOn;
    private final boolean isActive;
    private final BigDecimal annualFee;
    private final BigDecimal monthlyFee;
}
