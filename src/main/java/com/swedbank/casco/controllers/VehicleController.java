package com.swedbank.casco.controllers;

import com.google.common.base.CaseFormat;
import com.swedbank.casco.dtos.VehicleDto;
import com.swedbank.casco.dtos.VehicleListDto;
import com.swedbank.casco.models.InsuranceFee;
import com.swedbank.casco.models.SettingType;
import com.swedbank.casco.services.InsuranceFeeService;
import com.swedbank.casco.services.SettingService;
import com.swedbank.casco.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.swedbank.casco.utils.CascoUtils.getSortOfColumn;
import static com.swedbank.casco.utils.Constants.Data.DEFAULT_ITEMS_PER_PAGE;

/**
 * Controller to handle vehicle related requests
 *
 * @author Vinod John
 */
@RestController
@RequestMapping("/vehicle")
public class VehicleController {
    @Autowired
    private InsuranceFeeService insuranceFeeService;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private SettingService settingService;

    @GetMapping
    public ResponseEntity<VehicleListDto> getVehiclesByPageAndItems(
            @RequestParam(name = "page", defaultValue = "0") int pageNum,
            @RequestParam(name = "items", defaultValue = DEFAULT_ITEMS_PER_PAGE) int totalItem,
            @RequestParam(name = "sort", defaultValue = "createdDate") String sort,
            @RequestParam(name = "order", defaultValue = "desc") String order) {
        Page<InsuranceFee> insuranceFeePage = insuranceFeeService.findAllInsuranceFee(PageRequest.of(pageNum, totalItem,
                getSortOfColumn(sort, order)));
        List<VehicleDto> vehicleDtos = new ArrayList<>();
        insuranceFeePage.forEach(insuranceFee -> vehicleDtos.add(insuranceFee.vehicleDto()));
        VehicleListDto vehicleListDto = new VehicleListDto(vehicleDtos, pageNum, insuranceFeePage.getTotalElements());
        return new ResponseEntity<>(vehicleListDto, HttpStatus.OK);
    }

    @GetMapping("/fields")
    public List<String> getAllValidVehicleFields(@RequestParam(name = "type", defaultValue = "ALL") String type) {
        List<String> vehicleFields = vehicleService.getVehicleClassFieldsOfNumericType().stream()
                .map(field -> CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, field.getName()))
                .collect(Collectors.toList());

        if (type.equalsIgnoreCase("ALL")) {
            return vehicleFields;
        } else {
            return vehicleFields.stream()
                    .filter(s -> settingService.findSettingByTypeAndName(SettingType.valueOf(type), s).isEmpty())
                    .collect(Collectors.toList());
        }
    }
}
