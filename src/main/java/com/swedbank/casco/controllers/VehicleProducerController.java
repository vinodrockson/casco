package com.swedbank.casco.controllers;

import com.swedbank.casco.dtos.VehicleProducerDto;
import com.swedbank.casco.dtos.VehicleProducerListDto;
import com.swedbank.casco.exceptions.VehicleProducerNotFoundException;
import com.swedbank.casco.models.VehicleProducer;
import com.swedbank.casco.services.VehicleProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.swedbank.casco.utils.CascoUtils.getSortOfColumn;
import static com.swedbank.casco.utils.Constants.Data.DEFAULT_ITEMS_PER_PAGE;
import static com.swedbank.casco.utils.Constants.Data.DEFAULT_MAKE_COEFFICIENT;

/**
 * Controller to handle Vehicle Producer requests
 *
 * @author Vinod John
 */
@RestController
@RequestMapping("/vehicle-producer")
public class VehicleProducerController {
    @Autowired
    private VehicleProducerService vehicleProducerService;

    @GetMapping
    public ResponseEntity<VehicleProducerListDto> getVehicleProducersByPageAndItems(
            @RequestParam(name = "page", defaultValue = "0") int pageNum,
            @RequestParam(name = "items", defaultValue = DEFAULT_ITEMS_PER_PAGE) int totalItem,
            @RequestParam(name = "sort", defaultValue = "createdDate") String sort,
            @RequestParam(name = "order", defaultValue = "desc") String order) {
        Page<VehicleProducer> vehicleProducerPage = vehicleProducerService.findAllVehicleProducers(
                PageRequest.of(pageNum, totalItem, getSortOfColumn(sort, order)));
        List<VehicleProducer> vehicleProducers = new ArrayList<>();
        vehicleProducerPage.forEach(vehicleProducers::add);
        VehicleProducerListDto vehicleProducerListDto =
                new VehicleProducerListDto(vehicleProducers, pageNum, vehicleProducerPage.getTotalElements());
        return new ResponseEntity<>(vehicleProducerListDto, HttpStatus.OK);
    }

    @GetMapping("/active")
    public List<VehicleProducerDto> getActiveVehicleProducers() {
        return vehicleProducerService.getActiveVehicleProducers().stream()
                .map(VehicleProducer::vehicleProducerDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<?> createVehicleProducer(@Valid @RequestBody VehicleProducer vehicleProducer) {
        if (0 >= vehicleProducer.getRiskCoefficient()) {
            vehicleProducer.setRiskCoefficient(DEFAULT_MAKE_COEFFICIENT);
        }

        vehicleProducerService.createVehicleProducer(vehicleProducer);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> updateVehicleProducer(@Valid @RequestBody VehicleProducer vehicleProducer)
            throws VehicleProducerNotFoundException {
        if (0 >= vehicleProducer.getRiskCoefficient()) {
            vehicleProducer.setRiskCoefficient(DEFAULT_MAKE_COEFFICIENT);
        }

        vehicleProducerService.updateVehicleProducer(vehicleProducer);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteVehicleProducer(@PathVariable UUID id) throws VehicleProducerNotFoundException {
        vehicleProducerService.deleteVehicleProducerById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/restore/{id}")
    public ResponseEntity<?> restoreVehicleProducer(@PathVariable UUID id) throws VehicleProducerNotFoundException {
        vehicleProducerService.restoreVehicleProducerById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
