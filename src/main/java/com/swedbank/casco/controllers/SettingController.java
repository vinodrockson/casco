package com.swedbank.casco.controllers;

import com.swedbank.casco.dtos.SettingListDto;
import com.swedbank.casco.exceptions.SettingNotFoundException;
import com.swedbank.casco.models.Setting;
import com.swedbank.casco.models.SettingType;
import com.swedbank.casco.services.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.swedbank.casco.utils.CascoUtils.getSortOfColumn;
import static com.swedbank.casco.utils.Constants.Data.DEFAULT_ITEMS_PER_PAGE;

/**
 * Controller to handle setting related operations
 *
 * @author Vinod John
 */
@RestController
@RequestMapping("/setting")
public class SettingController {
    @Autowired
    private SettingService settingService;

    @GetMapping
    public ResponseEntity<SettingListDto> getSortedSettingByPageAndType(
            @RequestParam(name = "type", defaultValue = "RISK_COEFFICIENT") SettingType settingType,
            @RequestParam(name = "page", defaultValue = "0") int pageNum,
            @RequestParam(name = "items", defaultValue = DEFAULT_ITEMS_PER_PAGE) int totalItem,
            @RequestParam(name = "sort", defaultValue = "createdDate") String sort,
            @RequestParam(name = "order", defaultValue = "desc") String order) {
        Page<Setting> settingPage = settingService.findAllSettings(PageRequest.of(pageNum, totalItem,
                getSortOfColumn(sort, order)));
        List<Setting> settingList = settingPage.stream()
                .filter(setting -> setting.getSettingType().equals(settingType))
                .collect(Collectors.toList());
        SettingListDto settingListDto = new SettingListDto(settingList, pageNum, settingPage.getTotalElements());
        return new ResponseEntity<>(settingListDto, HttpStatus.OK);
    }

    @GetMapping("/type")
    public List<SettingType> getAllSettingType() {
        return Arrays.asList(SettingType.values().clone());
    }

    @PostMapping
    public ResponseEntity<?> createSetting(@RequestBody Setting setting) {
        settingService.createSetting(setting);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> updateSetting(@RequestBody Setting setting) throws SettingNotFoundException {
        settingService.updateSetting(setting);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteSetting(@PathVariable UUID id) throws SettingNotFoundException {
        settingService.deleteSettingById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/restore/{id}")
    public ResponseEntity<?> restoreSetting(@PathVariable UUID id) throws SettingNotFoundException {
        settingService.restoreSettingById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
