package com.swedbank.casco.controllers;

import com.swedbank.casco.dtos.InsuranceFeeDto;
import com.swedbank.casco.dtos.RiskDto;
import com.swedbank.casco.models.InsuranceFee;
import com.swedbank.casco.models.SettingType;
import com.swedbank.casco.models.Vehicle;
import com.swedbank.casco.services.InsuranceFeeService;
import com.swedbank.casco.services.SettingService;
import com.swedbank.casco.services.VehicleProducerService;
import com.swedbank.casco.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller to handle insurance fee requests
 *
 * @author Vinod John
 */
@RestController
@RequestMapping("/insurance-fee")
public class InsuranceFeeController {
    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private InsuranceFeeService insuranceFeeService;

    @Autowired
    private VehicleProducerService vehicleProducerService;

    @Autowired
    private SettingService settingService;

    @PostMapping
    public ResponseEntity<InsuranceFeeDto> calculateInsuranceFee(
            @Valid @RequestBody Vehicle vehicle,
            @RequestParam(name = "pi", defaultValue = "true") boolean isPreviousIndemnityAdded)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        vehicleProducerService.findVehicleProducerById(vehicle.getVehicleProducer().getId())
                .ifPresent(vehicle::setVehicleProducer);
        vehicleService.createVehicle(vehicle);
        InsuranceFeeDto insuranceFeeDto =
                translateInsuranceFee(insuranceFeeService.calculateFee(vehicle, isPreviousIndemnityAdded));
        return new ResponseEntity<>(insuranceFeeDto, HttpStatus.CREATED);
    }

    // PRIVATE METHODS //
    private InsuranceFeeDto translateInsuranceFee(InsuranceFee insuranceFee) {
        List<RiskDto> riskDtos = insuranceFee.getCalculatedRisks().stream()
                .map(setting -> new RiskDto(setting.getId(), setting.getName().replace("_", " "),
                        true))
                .collect(Collectors.toList());

        settingService.getActiveSettingsByType(SettingType.RISK_COEFFICIENT).forEach(setting -> {
            if (!insuranceFee.getCalculatedRisks().contains(setting)) {
                riskDtos.add(new RiskDto(setting.getId(), setting.getName().replace("_", " "),
                        false));
            }
        });

        return InsuranceFeeDto.builder()
                .id(insuranceFee.getId())
                .annualFee(insuranceFee.getAnnualFee())
                .monthlyFee(insuranceFee.getMonthlyFee())
                .firstRegistrationYear(insuranceFee.getVehicle().getFirstRegistrationYear())
                .mileage(insuranceFee.getVehicle().getMileage())
                .previousIndemnity(insuranceFee.getVehicle().getPreviousIndemnity())
                .vehicleProducer(insuranceFee.getVehicle().getVehicleProducer().getName())
                .feeCalculatedDate(insuranceFee.getLastModifiedDate())
                .risks(riskDtos)
                .build();
    }
}
