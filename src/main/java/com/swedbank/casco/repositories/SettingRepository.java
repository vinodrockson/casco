package com.swedbank.casco.repositories;

import com.swedbank.casco.models.Setting;
import com.swedbank.casco.models.SettingType;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * Repository to handle Settings related data queries
 *
 * @author Vinod John
 */
@Repository
public interface SettingRepository extends PagingAndSortingRepository<Setting, UUID> {
    Optional<Setting> findBySettingTypeAndName(SettingType settingType, String name);
}
