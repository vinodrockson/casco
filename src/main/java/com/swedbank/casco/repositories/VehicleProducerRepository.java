package com.swedbank.casco.repositories;

import com.swedbank.casco.models.VehicleProducer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * Repository to handle Vehicle Producer related data queries
 *
 * @author Vinod John
 */
@Repository
public interface VehicleProducerRepository extends PagingAndSortingRepository<VehicleProducer, UUID> {
    Optional<VehicleProducer> findByName(String name);
}
