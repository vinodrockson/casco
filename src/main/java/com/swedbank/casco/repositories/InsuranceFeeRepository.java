package com.swedbank.casco.repositories;

import com.swedbank.casco.models.InsuranceFee;
import com.swedbank.casco.models.Vehicle;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * Repository to handle Insurance Fee related data queries
 *
 * @author Vinod John
 */
@Repository
public interface InsuranceFeeRepository extends PagingAndSortingRepository<InsuranceFee, UUID> {
    Optional<InsuranceFee> findByVehicle(Vehicle vehicle);
}
