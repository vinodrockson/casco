package com.swedbank.casco.repositories;

import com.swedbank.casco.models.Vehicle;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * Repository to handle Vehicle related data queries
 *
 * @author Vinod John
 */
@Repository
public interface VehicleRepository extends PagingAndSortingRepository<Vehicle, UUID> {
    Optional<Vehicle> findByPlateNumber(String plateNumber);
}
