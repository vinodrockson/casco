package com.swedbank.casco.utils;

/**
 * Constant values used in this application
 *
 * @author Vinod John
 */
public class Constants {
    public static class Data {
        public static final Double DEFAULT_MAKE_COEFFICIENT = 1D;
        public static final String DEFAULT_ITEMS_PER_PAGE = "10";
    }

    public static class Message {
        public static final String RECORD_IMPORT_SKIPPED = "Import skipped for record number:";
        public static final String INVALID_PLATE_NUMBER = "Invalid plate number!";
        public static final String INVALID_PURCHASE_PRICE = "Invalid purchase price!";
        public static final String INVALID_VEHICLE_PRODUCER = "Invalid vehicle producer!";
        public static final String INVALID_MILEAGE = "Invalid mileage!";
        public static final String INVALID_PREVIOUS_INDEMNITY = "Invalid previous indemnity!";
        public static final String INVALID_REG_YEAR_LESSER =
                "Invalid registration year! Year should not be lesser than ";
        public static final String INVALID_REG_YEAR_GREATER =
                "Invalid registration year! Year cannot be greater than the present year.";
    }

    public static class Audit {
        public static final String DEFAULT_AUDITOR = "SYSTEM";
    }

    public static class Validation {
        public static final int FEE_CALCULATION_YEARS = 50;
        public static final double DEFAULT_NEGATIVE_ANNUAL_FEE = 0;
    }
}
