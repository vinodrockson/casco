package com.swedbank.casco.utils.constraints;

import com.swedbank.casco.models.Vehicle;
import com.swedbank.casco.services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Constraint validator to check vehicle is valid
 *
 * @author VinodJohn
 */
@Component
public class VehicleValidator implements ConstraintValidator<ValidVehicle, Vehicle> {
    @Autowired
    private ValidationService validationService;

    @Override
    public void initialize(ValidVehicle constraintAnnotation) {
    }

    @Override
    public boolean isValid(Vehicle vehicle, ConstraintValidatorContext constraintValidatorContext) {
        try {
            validationService.validateVehicle(vehicle);
            return true;
        } catch (RuntimeException e) {
            if (!e.getMessage().isBlank()) {
                constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                        .addConstraintViolation()
                        .disableDefaultConstraintViolation();
            }
        }
        return false;
    }
}
