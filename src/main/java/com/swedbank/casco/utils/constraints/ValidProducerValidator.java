package com.swedbank.casco.utils.constraints;

import com.swedbank.casco.models.VehicleProducer;
import com.swedbank.casco.services.VehicleProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

/**
 * Constraint validator to check if vehicle producer already exists
 *
 * @author Vinod John
 */
@Component
public class ValidProducerValidator implements ConstraintValidator<ValidProducer, VehicleProducer> {
    @Autowired
    private VehicleProducerService vehicleProducerService;

    @Override
    public void initialize(ValidProducer constraintAnnotation) {

    }

    @Override
    public boolean isValid(VehicleProducer vehicleProducer, ConstraintValidatorContext constraintValidatorContext) {
        String producer = vehicleProducer.getName().trim().replaceAll("\\s", "_").toUpperCase();
        Optional<VehicleProducer> optionalValidProducer = vehicleProducerService.findVehicleProducerByName(producer);

        if (vehicleProducer.getId() == null) {
            return optionalValidProducer.isEmpty();
        } else {
            return optionalValidProducer.isPresent();
        }
    }
}
