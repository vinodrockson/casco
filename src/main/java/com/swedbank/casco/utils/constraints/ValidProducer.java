package com.swedbank.casco.utils.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Constraint annotation for unique producer validation
 *
 * @author Vinod John
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Constraint(validatedBy = {ValidProducerValidator.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidProducer {
    String message() default "{messages.constraints.producer-already-exists}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
