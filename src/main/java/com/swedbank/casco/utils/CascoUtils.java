package com.swedbank.casco.utils;

import org.springframework.data.domain.Sort;

/**
 * A helper class to provide common functionalities for this app
 *
 * @author Vinod John
 */
public class CascoUtils {
    public static Sort getSortOfColumn(String sort, String order) {
        if (order.toLowerCase().equals("asc")) {
            return Sort.by(sort).ascending();
        } else {
            return Sort.by(sort).descending();
        }
    }
}
