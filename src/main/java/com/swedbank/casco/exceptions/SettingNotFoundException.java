package com.swedbank.casco.exceptions;

import java.util.UUID;

/**
 * Exception to handle Setting's unavailability
 *
 * @author Vinod John
 */
public class SettingNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public SettingNotFoundException(UUID id) {
        super("Setting not found! (ID: " + id.toString());
    }
}
