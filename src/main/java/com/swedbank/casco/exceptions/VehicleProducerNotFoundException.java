package com.swedbank.casco.exceptions;

import java.util.UUID;

/**
 * Exception to handle Vehicle Producer's unavailability
 *
 * @author Vinod John
 */
public class VehicleProducerNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public VehicleProducerNotFoundException(UUID id, String name) {
        super("Vehicle Producer not found! (ID: " + id.toString() + ", Name: " + name + ")");
    }
}
