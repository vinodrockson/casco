package com.swedbank.casco.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swedbank.casco.dtos.VehicleProducerDto;
import com.swedbank.casco.utils.constraints.ValidProducer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * Vehicle Producer model
 *
 * @author Vinod John
 */
@Data
@Entity
@ValidProducer
@EqualsAndHashCode(callSuper = true)
public class VehicleProducer extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "UUID")
    @Column(updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotBlank(message = "{messages.constraints.blank-vehicle-producer-name}")
    private String name;

    @Min(message = "{messages.constraints.minimum-purchase-price}", value = 1)
    private BigDecimal averagePurchasePrice;

    private Double riskCoefficient;

    @JsonProperty("isActive")
    private boolean isActive;

    public VehicleProducerDto vehicleProducerDto() {
        return VehicleProducerDto.builder()
                .id(id)
                .name(name)
                .build();
    }
}
