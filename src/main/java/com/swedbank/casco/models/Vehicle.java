package com.swedbank.casco.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swedbank.casco.utils.constraints.ValidVehicle;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * Vehicle model
 *
 * @author Vinod John
 */
@Data
@Entity
@ValidVehicle
@EqualsAndHashCode(callSuper = true)
public class Vehicle extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @Column(updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    private String plateNumber;
    private int firstRegistrationYear;
    private BigDecimal purchasePrice;

    @OneToOne(cascade = {CascadeType.MERGE})
    private VehicleProducer vehicleProducer;

    private Long mileage;
    private BigDecimal previousIndemnity;

    @JsonProperty("isActive")
    private boolean isActive;
}
