package com.swedbank.casco.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.swedbank.casco.dtos.VehicleDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * Insurance Fee model
 *
 * @author Vinod John
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class InsuranceFee extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @Column(updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @OneToOne(cascade = {CascadeType.MERGE})
    private Vehicle vehicle;

    private BigDecimal annualFee;
    private BigDecimal monthlyFee;

    @ManyToMany
    private List<Setting> calculatedRisks;

    public VehicleDto vehicleDto() {
        return VehicleDto.builder()
                .id(vehicle.getId())
                .firstRegistrationYear(vehicle.getFirstRegistrationYear())
                .mileage(vehicle.getMileage())
                .plateNumber(vehicle.getPlateNumber())
                .previousIndemnity(vehicle.getPreviousIndemnity())
                .purchasePrice(vehicle.getPurchasePrice())
                .vehicleProducer(vehicle.getVehicleProducer().getName())
                .isActive(vehicle.isActive())
                .createdOn(vehicle.getCreatedDate().toString())
                .annualFee(annualFee)
                .monthlyFee(monthlyFee)
                .build();
    }
}
