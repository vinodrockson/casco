package com.swedbank.casco.models;

/**
 * Types of Setting
 *
 * @author Vinod John
 */
public enum SettingType {
    RISK_COEFFICIENT
}
