package com.swedbank.casco.services;

import com.swedbank.casco.models.Vehicle;
import com.swedbank.casco.models.VehicleProducer;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Service to validate vehicle data
 *
 * @author Vinod John
 */
public interface ValidationService {
    /**
     * To check whether Vehicle is valid or nor
     *
     * @param vehicle Vehicle
     */
    void validateVehicle(Vehicle vehicle);

    /**
     * To check whether the plate number is valid or not
     *
     * @param plateNumber Vehicle's plate number
     */
    void validatePlateNumber(String plateNumber);

    /**
     * To check whether the price is valid or not
     *
     * @param price Vehicle's purchase price
     */
    void validatePurchasePrice(BigDecimal price);

    /**
     * To check whether the VehicleProducer is valid or not
     *
     * @param vehicleProducer Vehicle's vehicleProducer
     * @return Vehicle Producer
     */
    VehicleProducer validateVehicleProducer(String vehicleProducer);

    /**
     * To check whether the VehicleProducer is valid or not
     *
     * @param id Vehicle Producer's id
     * @return Vehicle Producer
     */
    VehicleProducer validateVehicleProducer(UUID id);

    /**
     * To check whether the mileage is valid or not
     *
     * @param mileage Vehicle's mileage
     */
    void validateMileage(Long mileage);

    /**
     * To check whether the previous indemnity is valid or not
     *
     * @param previousIndemnity Vehicle's previousIndemnity
     */
    void validatePreviousIndemnity(BigDecimal previousIndemnity);

    /**
     * To check whether the registration year is valid or not
     *
     * @param regYear Vehicle's registration year
     */
    void validateRegistrationYear(int regYear);
}
