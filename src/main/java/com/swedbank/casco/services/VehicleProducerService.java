package com.swedbank.casco.services;

import com.swedbank.casco.exceptions.VehicleProducerNotFoundException;
import com.swedbank.casco.models.VehicleProducer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service to handle Vehicle Producer related operations
 *
 * @author Vinod John
 */
public interface VehicleProducerService {
    /**
     * To create a new vehicle producer
     *
     * @param vehicleProducer Vehicle Producer
     */
    void createVehicleProducer(VehicleProducer vehicleProducer);

    /**
     * To find a Vehicle Producer by its name
     *
     * @param name Name of a producer
     * @return optional of a Producer
     */
    Optional<VehicleProducer> findVehicleProducerByName(String name);

    /**
     * To find Vehicle Producer by id
     *
     * @param id Vehicle Producer's id
     * @return optional of a producer
     */
    Optional<VehicleProducer> findVehicleProducerById(UUID id);

    /**
     * To get all vehicle producers
     *
     * @return page of vehicle Producers
     */
    Page<VehicleProducer> findAllVehicleProducers(Pageable pageable);

    /**
     * To get list of active vehicle producers
     *
     * @return list of all vehicle Producers
     */
    List<VehicleProducer> getActiveVehicleProducers();

    /**
     * To update an existing vehicle producer
     *
     * @param vehicleProducer Vehicle Producer
     */
    void updateVehicleProducer(VehicleProducer vehicleProducer) throws VehicleProducerNotFoundException;

    /**
     * To create a Vehicle Producer if not exists
     *
     * @param name Producer name
     * @return Vehicle Producer
     */
    VehicleProducer createVehicleProducerIfNotExists(String name);

    /**
     * To delete vehicle producer by Id
     *
     * @param id Producer's id
     */
    void deleteVehicleProducerById(UUID id) throws VehicleProducerNotFoundException;

    /**
     * To restore vehicle producer by Id
     *
     * @param id Producer's id
     */
    void restoreVehicleProducerById(UUID id) throws VehicleProducerNotFoundException;
}
