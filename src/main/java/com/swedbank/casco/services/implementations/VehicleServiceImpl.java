package com.swedbank.casco.services.implementations;

import com.swedbank.casco.models.Vehicle;
import com.swedbank.casco.repositories.VehicleRepository;
import com.swedbank.casco.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of Vehicle service
 *
 * @author Vinod John
 */
@Service
@Transactional
public class VehicleServiceImpl implements VehicleService {
    @Autowired
    private VehicleRepository vehicleRepository;

    @Override
    public void createVehicle(Vehicle vehicle) {
        vehicle.setPlateNumber(vehicle.getPlateNumber().toUpperCase());
        vehicle.setActive(true);
        vehicleRepository.save(vehicle);
    }

    @Override
    public Optional<Vehicle> findByPlateNumber(String plateNumber) {
        return vehicleRepository.findByPlateNumber(plateNumber);
    }

    @Override
    public List<Field> getVehicleClassFieldsOfNumericType() {


        List<Field> fieldList = Arrays.stream(Vehicle.class.getDeclaredFields())
                .filter(field -> !Modifier.isStatic(field.getModifiers())
                        && isDataTypeValid(field.getType().getSimpleName()))
                .collect(Collectors.toList());

        return fieldList;
    }

    // PRIVATE METHODS //
    private boolean isDataTypeValid(String type) {
        List<String> numericClasses = Arrays.asList(BigDecimal.class.getSimpleName(), Integer.class.getSimpleName(),
                Double.class.getSimpleName(), Float.class.getSimpleName(), Long.class.getSimpleName(),
                int.class.getSimpleName(), float.class.getSimpleName(), double.class.getSimpleName(),
                long.class.getSimpleName());

        return numericClasses.stream()
                .anyMatch(t -> t.equals(type));
    }
}
