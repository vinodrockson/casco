package com.swedbank.casco.services.implementations;

import com.swedbank.casco.models.Vehicle;
import com.swedbank.casco.models.VehicleProducer;
import com.swedbank.casco.services.ValidationService;
import com.swedbank.casco.services.VehicleProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

import static com.swedbank.casco.utils.Constants.Message.*;
import static com.swedbank.casco.utils.Constants.Validation.FEE_CALCULATION_YEARS;

/**
 * Implementation of Validation Service
 *
 * @author Vinod John
 */
@Service
public class ValidationServiceImpl implements ValidationService {
    @Autowired
    private VehicleProducerService vehicleProducerService;

    @Override
    public void validateVehicle(Vehicle vehicle) {
        validatePlateNumber(vehicle.getPlateNumber());
        validateMileage(vehicle.getMileage());
        validatePurchasePrice(vehicle.getPurchasePrice());
        validateRegistrationYear(vehicle.getFirstRegistrationYear());

        if (vehicle.getVehicleProducer().getId() != null) {
            validateVehicleProducer(vehicle.getVehicleProducer().getId());
        } else {
            validateVehicleProducer(vehicle.getVehicleProducer().getName());
        }

        validatePreviousIndemnity(vehicle.getPreviousIndemnity());
    }

    @Override
    public void validatePlateNumber(String plateNumber) {
        if (!(6 == plateNumber.length() && Pattern.matches("[0-9]{3}[A-Za-z]{3}", plateNumber))) {
            throw new RuntimeException(INVALID_PLATE_NUMBER);
        }
    }

    @Override
    public void validatePurchasePrice(BigDecimal price) {
        if (0 <= BigDecimal.ZERO.compareTo(price)) {
            throw new RuntimeException(INVALID_PURCHASE_PRICE);
        }
    }

    @Override
    public VehicleProducer validateVehicleProducer(String vehicleProducer) {
        Optional<VehicleProducer> vehicleProducerOptional = vehicleProducerService
                .findVehicleProducerByName(vehicleProducer);

        if (vehicleProducerOptional.isEmpty()) {
            throw new RuntimeException(INVALID_VEHICLE_PRODUCER);
        }

        return vehicleProducerOptional.get();
    }

    @Override
    public VehicleProducer validateVehicleProducer(UUID id) {
        Optional<VehicleProducer> vehicleProducerOptional = vehicleProducerService.findVehicleProducerById(id);

        if (vehicleProducerOptional.isEmpty()) {
            throw new RuntimeException(INVALID_VEHICLE_PRODUCER);
        }

        return vehicleProducerOptional.get();
    }

    @Override
    public void validateMileage(Long mileage) {
        if (0 > mileage) {
            throw new RuntimeException(INVALID_MILEAGE);
        }
    }

    @Override
    public void validatePreviousIndemnity(BigDecimal previousIndemnity) {
        if (0 < BigDecimal.ZERO.compareTo(previousIndemnity)) {
            throw new RuntimeException(INVALID_PREVIOUS_INDEMNITY);
        }
    }

    @Override
    public void validateRegistrationYear(int regYear) {
        int minimumValidYear = LocalDate.now().minusYears(FEE_CALCULATION_YEARS).getYear();

        if (regYear < minimumValidYear) {
            throw new RuntimeException(INVALID_REG_YEAR_LESSER + minimumValidYear);
        } else if (regYear > LocalDate.now().getYear()) {
            throw new RuntimeException(INVALID_REG_YEAR_GREATER);
        }
    }
}
