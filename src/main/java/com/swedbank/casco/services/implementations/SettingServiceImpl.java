package com.swedbank.casco.services.implementations;

import com.google.common.collect.ImmutableList;
import com.swedbank.casco.exceptions.SettingNotFoundException;
import com.swedbank.casco.models.Setting;
import com.swedbank.casco.models.SettingType;
import com.swedbank.casco.repositories.SettingRepository;
import com.swedbank.casco.services.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Implementation of Setting Service
 *
 * @author Vinod John
 */
@Service
@Transactional
public class SettingServiceImpl implements SettingService {
    @Autowired
    private SettingRepository settingRepository;

    @Override
    public void createSetting(Setting setting) {
        String name = setting.getName().trim().replaceAll("\\s", "_").toUpperCase();
        setting.setName(name);
        settingRepository.save(setting);
    }

    @Override
    public Optional<Setting> findSettingByTypeAndName(SettingType settingType, String name) {
        return settingRepository.findBySettingTypeAndName(settingType, name);
    }

    @Override
    public List<Setting> getActiveSettingsByType(SettingType settingType) {
        return ImmutableList.copyOf(settingRepository.findAll()).stream()
                .filter(Setting::isActive)
                .filter(setting -> setting.getSettingType().equals(settingType))
                .collect(Collectors.toList());
    }

    @Override
    public Page<Setting> findAllSettings(Pageable pageable) {
        return settingRepository.findAll(pageable);
    }

    @Override
    public void updateSetting(Setting setting) throws SettingNotFoundException {
        Optional<Setting> optionalSetting = settingRepository.findById(setting.getId());

        if (optionalSetting.isEmpty()) {
            throw new SettingNotFoundException(setting.getId());
        }

        settingRepository.save(setting);
    }

    @Override
    public void deleteSettingById(UUID id) throws SettingNotFoundException {
        Optional<Setting> optionalSetting = settingRepository.findById(id);

        if (optionalSetting.isEmpty()) {
            throw new SettingNotFoundException(id);
        }

        Setting setting = optionalSetting.get();
        setting.setActive(false);
        settingRepository.save(setting);
    }

    @Override
    public void restoreSettingById(UUID id) throws SettingNotFoundException {
        Optional<Setting> optionalSetting = settingRepository.findById(id);

        if (optionalSetting.isEmpty()) {
            throw new SettingNotFoundException(id);
        }

        Setting setting = optionalSetting.get();
        setting.setActive(true);
        settingRepository.save(setting);
    }
}
