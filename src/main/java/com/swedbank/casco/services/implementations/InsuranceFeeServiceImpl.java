package com.swedbank.casco.services.implementations;

import com.google.common.base.CaseFormat;
import com.swedbank.casco.models.InsuranceFee;
import com.swedbank.casco.models.Setting;
import com.swedbank.casco.models.Vehicle;
import com.swedbank.casco.repositories.InsuranceFeeRepository;
import com.swedbank.casco.services.InsuranceFeeService;
import com.swedbank.casco.services.SettingService;
import com.swedbank.casco.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.swedbank.casco.models.SettingType.RISK_COEFFICIENT;
import static com.swedbank.casco.utils.Constants.Validation.DEFAULT_NEGATIVE_ANNUAL_FEE;

/**
 * Implementation of InsuranceFee Service
 *
 * @author Vinod John
 */
@Service
@Transactional
public class InsuranceFeeServiceImpl implements InsuranceFeeService {
    private final static String PREVIOUS_INDEMNITY_RISK = "PREVIOUS_INDEMNITY";

    @Autowired
    private InsuranceFeeRepository insuranceFeeRepository;

    @Autowired
    private SettingService settingService;

    @Autowired
    private VehicleService vehicleService;

    @Override
    public InsuranceFee calculateFee(Vehicle vehicle, boolean doCalculatePreviousIndemnity)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        List<Setting> calculatedRisks = settingService.getActiveSettingsByType(RISK_COEFFICIENT).stream()
                .filter(setting -> !(setting.getName().equals(PREVIOUS_INDEMNITY_RISK) && !doCalculatePreviousIndemnity))
                .filter(setting -> !(setting.getName().equals(PREVIOUS_INDEMNITY_RISK)
                        && 0 == BigDecimal.ZERO.compareTo(vehicle.getPreviousIndemnity())))
                .collect(Collectors.toList());

        BigDecimal annualFee = getAnnualFee(vehicle, calculatedRisks).setScale(2, RoundingMode.CEILING);

        /* Sometimes the given formula returns negative fee value for wrong input which in turn replaced
        by default value */
        annualFee = 0 < BigDecimal.ZERO.compareTo(annualFee) ? BigDecimal.valueOf(DEFAULT_NEGATIVE_ANNUAL_FEE)
                : annualFee;

        BigDecimal monthlyFee = 0 == BigDecimal.ZERO.compareTo(annualFee) ? annualFee
                : annualFee.divide(BigDecimal.valueOf(12), RoundingMode.DOWN);

        InsuranceFee insuranceFee = new InsuranceFee();
        insuranceFee.setVehicle(vehicle);
        insuranceFee.setAnnualFee(annualFee);
        insuranceFee.setMonthlyFee(monthlyFee);
        insuranceFee.setCalculatedRisks(calculatedRisks);
        return insuranceFeeRepository.save(insuranceFee);
    }

    @Override
    public Optional<InsuranceFee> findInsuranceFeeByPlateNumber(String plateNumber) {
        Optional<Vehicle> vehicleOptional = vehicleService.findByPlateNumber(plateNumber);

        if (vehicleOptional.isPresent()) {
            return insuranceFeeRepository.findByVehicle(vehicleOptional.get());
        }

        return Optional.empty();
    }

    @Override
    public Page<InsuranceFee> findAllInsuranceFee(Pageable pageable) {
        return insuranceFeeRepository.findAll(pageable);
    }

    // PRIVATE METHODS //
    private BigDecimal getAnnualFee(Vehicle vehicle, List<Setting> calculatedRisks)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        BigDecimal calculatedRiskSum = BigDecimal.ZERO;
        int vehicleAge = LocalDate.now().getYear() - vehicle.getFirstRegistrationYear();

        for (Setting setting : calculatedRisks) {
            switch (setting.getName()) {
                case "VEHICLE_AGE":
                    calculatedRiskSum = calculatedRiskSum
                            .add(BigDecimal.valueOf(getRiskCoefficient(setting) * vehicleAge));
                    break;
                case "VEHICLE_VALUE":
                    BigDecimal vehicleCurrentValue = getVehicleCurrentValue(vehicleAge, vehicle.getMileage(),
                            vehicle.getPurchasePrice());
                    calculatedRiskSum = calculatedRiskSum
                            .add(BigDecimal.valueOf(getRiskCoefficient(setting)).multiply(vehicleCurrentValue));
                    break;
                default:
                    calculatedRiskSum = calculatedRiskSum
                            .add(BigDecimal.valueOf(getRiskCoefficient(setting))
                                    .multiply(getValueForParameterOfVehicle(vehicle, setting.getName())));
                    break;
            }
        }

        return BigDecimal.valueOf(vehicle.getVehicleProducer().getRiskCoefficient()).multiply(calculatedRiskSum);
    }

    private BigDecimal getVehicleCurrentValue(int age, Long mileage, BigDecimal purchasePrice) {
        double currentPercentValue = 102 + (-7.967) * age + (0.8337334) * (age ^ 2) + (-0.07785488) * (age ^ 3)
                + (0.002518395) * (age ^ 4) + (-0.0002236396) * mileage + (3.669157e-10) * (mileage ^ 2)
                + (-1.813681e-16) * (mileage ^ 3);

        return purchasePrice.multiply(BigDecimal.valueOf(currentPercentValue / 100));
    }

    private double getRiskCoefficient(Setting setting) {
        return null == setting ? 0 : Double.parseDouble(setting.getValue());
    }

    private BigDecimal getValueForParameterOfVehicle(Vehicle vehicle, String parameter)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String methodName = "get".concat(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, parameter));
        Method method = vehicle.getClass().getMethod(methodName);
        return (BigDecimal) method.invoke(vehicle);
    }
}
