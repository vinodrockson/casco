package com.swedbank.casco.services.implementations;

import com.google.common.collect.ImmutableList;
import com.swedbank.casco.exceptions.VehicleProducerNotFoundException;
import com.swedbank.casco.models.VehicleProducer;
import com.swedbank.casco.repositories.VehicleProducerRepository;
import com.swedbank.casco.services.VehicleProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.swedbank.casco.utils.Constants.Data.DEFAULT_MAKE_COEFFICIENT;

/**
 * Implementation of Vehicle Producer service
 *
 * @author Vinod John
 */
@Service
@Transactional
public class VehicleProducerServiceImpl implements VehicleProducerService {
    @Autowired
    private VehicleProducerRepository vehicleProducerRepository;

    @Override
    public void createVehicleProducer(VehicleProducer vehicleProducer) {
        boolean isActive = 0 != BigDecimal.ZERO.compareTo(vehicleProducer.getAveragePurchasePrice());
        String name = vehicleProducer.getName().trim().replaceAll("\\s", "_").toUpperCase();

        vehicleProducer.setActive(isActive);
        vehicleProducer.setName(name);
        vehicleProducerRepository.save(vehicleProducer);
    }

    @Override
    public Optional<VehicleProducer> findVehicleProducerByName(String name) {
        return vehicleProducerRepository.findByName(name);
    }

    @Override
    public Optional<VehicleProducer> findVehicleProducerById(UUID id) {
        return vehicleProducerRepository.findById(id);
    }

    @Override
    public Page<VehicleProducer> findAllVehicleProducers(Pageable pageable) {
        return vehicleProducerRepository.findAll(pageable);
    }

    @Override
    public List<VehicleProducer> getActiveVehicleProducers() {
        return ImmutableList.copyOf(vehicleProducerRepository.findAll()).stream()
                .filter(VehicleProducer::isActive)
                .sorted(Comparator.comparing(VehicleProducer::getName))
                .collect(Collectors.toList());
    }

    @Override
    public void updateVehicleProducer(VehicleProducer vehicleProducer) throws VehicleProducerNotFoundException {
        if (findVehicleProducerById(vehicleProducer.getId()).isEmpty()) {
            throw new VehicleProducerNotFoundException(vehicleProducer.getId(), vehicleProducer.getName());
        }

        vehicleProducerRepository.save(vehicleProducer);
    }

    @Override
    public VehicleProducer createVehicleProducerIfNotExists(String name) {
        VehicleProducer vehicleProducer = findVehicleProducerByName(name).orElse(null);

        if (null == vehicleProducer) {
            vehicleProducer = new VehicleProducer();
            vehicleProducer.setName(name);
            vehicleProducer.setAveragePurchasePrice(BigDecimal.ZERO);
            vehicleProducer.setRiskCoefficient(DEFAULT_MAKE_COEFFICIENT);
            createVehicleProducer(vehicleProducer);
        }

        return vehicleProducer;
    }

    @Override
    public void deleteVehicleProducerById(UUID id) throws VehicleProducerNotFoundException {
        Optional<VehicleProducer> optionalVehicleProducer = findVehicleProducerById(id);

        if (optionalVehicleProducer.isEmpty()) {
            throw new VehicleProducerNotFoundException(id, null);
        }

        VehicleProducer vehicleProducer = optionalVehicleProducer.get();
        vehicleProducer.setActive(false);
        vehicleProducerRepository.save(vehicleProducer);
    }

    @Override
    public void restoreVehicleProducerById(UUID id) throws VehicleProducerNotFoundException {
        Optional<VehicleProducer> optionalVehicleProducer = findVehicleProducerById(id);

        if (optionalVehicleProducer.isEmpty()) {
            throw new VehicleProducerNotFoundException(id, null);
        }

        VehicleProducer vehicleProducer = optionalVehicleProducer.get();
        vehicleProducer.setActive(true);
        vehicleProducerRepository.save(vehicleProducer);
    }
}
