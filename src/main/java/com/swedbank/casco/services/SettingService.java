package com.swedbank.casco.services;

import com.swedbank.casco.exceptions.SettingNotFoundException;
import com.swedbank.casco.models.Setting;
import com.swedbank.casco.models.SettingType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service to handle setting related operations
 *
 * @author Vinod John
 */
public interface SettingService {
    /**
     * To create a new setting
     *
     * @param setting Setting
     */
    void createSetting(Setting setting);

    /**
     * To find a Setting by its Type and Name
     *
     * @param settingType Type of a Setting
     * @param name        Name of a Setting
     * @return optional of Setting
     */
    Optional<Setting> findSettingByTypeAndName(SettingType settingType, String name);

    /**
     * To get list of active settings by type
     *
     * @param settingType Type of Setting
     * @return list of setting
     */
    List<Setting> getActiveSettingsByType(SettingType settingType);

    /**
     * To find all settings
     *
     * @return page of setting
     */
    Page<Setting> findAllSettings(Pageable pageable);

    /**
     * To update an existing setting
     *
     * @param setting Setting setting
     */
    void updateSetting(Setting setting) throws SettingNotFoundException;

    /**
     * To delete setting by Id
     *
     * @param id Setting Id
     */
    void deleteSettingById(UUID id) throws SettingNotFoundException;

    /**
     * To restore setting by Id
     *
     * @param id Setting Id
     */
    void restoreSettingById(UUID id) throws SettingNotFoundException;
}

