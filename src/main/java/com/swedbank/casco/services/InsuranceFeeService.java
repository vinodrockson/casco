package com.swedbank.casco.services;

import com.swedbank.casco.models.InsuranceFee;
import com.swedbank.casco.models.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

/**
 * Service to handle insurance fee related operations
 *
 * @author Vinod John
 */
public interface InsuranceFeeService {
    /**
     * To calculate insurance fee of the given vehicle
     *
     * @param vehicle                      Vehicle
     * @param doCalculatePreviousIndemnity Flag to take previous indemnity risk in calculation
     * @return created InsuranceFee
     */
    InsuranceFee calculateFee(Vehicle vehicle, boolean doCalculatePreviousIndemnity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException;

    /**
     * To find insurance fee by plate number
     *
     * @param plateNumber Plate number of vehicle
     * @return Optional of InsuranceFee
     */
    Optional<InsuranceFee> findInsuranceFeeByPlateNumber(String plateNumber);

    /**
     * To find all insurance fee
     *
     * @param pageable Pageable
     * @return Page of InsuranceFee
     */
    Page<InsuranceFee> findAllInsuranceFee(Pageable pageable);
}
