package com.swedbank.casco.services;

import com.swedbank.casco.models.Vehicle;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

/**
 * Service to handle vehicle related operations
 *
 * @author Vinod John
 */
public interface VehicleService {
    /**
     * To create a new vehicle
     *
     * @param vehicle Vehicle
     */
    void createVehicle(Vehicle vehicle);

    /**
     * To find vehicle by plate number
     *
     * @param plateNumber Plate number of vehicle
     * @return Optional of Vehicle
     */
    Optional<Vehicle> findByPlateNumber(String plateNumber);

    /**
     * To get list of all number fields of vehicle class
     *
     * @return list of field
     */
    List<Field> getVehicleClassFieldsOfNumericType();
}
