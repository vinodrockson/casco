package com.swedbank.casco;

import com.swedbank.casco.dtos.InsuranceFeeDto;
import com.swedbank.casco.dtos.VehicleProducerDto;
import com.swedbank.casco.handlers.exception.ErrorResponse;
import com.swedbank.casco.models.Vehicle;
import com.swedbank.casco.models.VehicleProducer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.swedbank.casco.CascoTestUtils.createURLWithPort;

/**
 * Tests for Insurance Fee Controller endpoints
 *
 * @author Vinod John
 */
@RunWith(SpringRunner.class)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InsuranceFeeControllerTests {
    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private VehicleProducer vehicleProducer;

    @Before
    public void setup() {
        ResponseEntity<VehicleProducerDto[]> vpResponseEntity = restTemplate
                .getForEntity(createURLWithPort("/vehicle-producer/active", port), VehicleProducerDto[].class);
        List<VehicleProducerDto> vehicleProducerDtos = Arrays.asList(Objects.requireNonNull(vpResponseEntity.getBody()));

        vehicleProducer = new VehicleProducer();
        vehicleProducerDtos.stream()
                .findAny()
                .ifPresent(vehicleProducerDto -> vehicleProducer.setId(vehicleProducerDto.getId()));
    }

    @Test
    public void testCalculateInsuranceFeeWithPreviousIndemnity() {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleProducer(vehicleProducer);
        vehicle.setPlateNumber("345RTY");
        vehicle.setActive(true);
        vehicle.setPreviousIndemnity(BigDecimal.TEN);
        vehicle.setPurchasePrice(BigDecimal.valueOf(34567));
        vehicle.setMileage(145000L);
        vehicle.setFirstRegistrationYear(2018);

        HttpEntity<Vehicle> entity = new HttpEntity<>(vehicle, new HttpHeaders());
        ResponseEntity<InsuranceFeeDto> insuranceFeeDtoResponseEntity = restTemplate
                .exchange(createURLWithPort("/insurance-fee?pi=true", port), HttpMethod.POST, entity,
                        InsuranceFeeDto.class);

        Assert.assertEquals(HttpStatus.CREATED, insuranceFeeDtoResponseEntity.getStatusCode());
        Assert.assertNotNull(insuranceFeeDtoResponseEntity.getBody());
        Assert.assertEquals(BigDecimal.valueOf(1484.26), insuranceFeeDtoResponseEntity.getBody().getAnnualFee());
        Assert.assertEquals(BigDecimal.valueOf(123.68), insuranceFeeDtoResponseEntity.getBody().getMonthlyFee());
    }

    @Test
    public void testCalculateInsuranceFeeWithoutPreviousIndemnity() {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleProducer(vehicleProducer);
        vehicle.setPlateNumber("345RTY");
        vehicle.setActive(true);
        vehicle.setPreviousIndemnity(BigDecimal.TEN);
        vehicle.setPurchasePrice(BigDecimal.valueOf(34567));
        vehicle.setMileage(145000L);
        vehicle.setFirstRegistrationYear(2018);

        HttpEntity<Vehicle> entity = new HttpEntity<>(vehicle, new HttpHeaders());
        ResponseEntity<InsuranceFeeDto> insuranceFeeDtoResponseEntity = restTemplate
                .exchange(createURLWithPort("/insurance-fee?pi=false", port), HttpMethod.POST, entity,
                        InsuranceFeeDto.class);

        Assert.assertEquals(HttpStatus.CREATED, insuranceFeeDtoResponseEntity.getStatusCode());
        Assert.assertNotNull(insuranceFeeDtoResponseEntity.getBody());
        Assert.assertEquals(BigDecimal.valueOf(1483.76), insuranceFeeDtoResponseEntity.getBody().getAnnualFee());
        Assert.assertEquals(BigDecimal.valueOf(123.64), insuranceFeeDtoResponseEntity.getBody().getMonthlyFee());
    }

    @Test
    public void testCalculateInsuranceFeeValidationException() {
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleProducer(vehicleProducer);
        vehicle.setPlateNumber("3K5RTY");
        vehicle.setActive(true);
        vehicle.setPreviousIndemnity(BigDecimal.ZERO);
        vehicle.setPurchasePrice(BigDecimal.valueOf(34567));
        vehicle.setMileage(145000L);
        vehicle.setFirstRegistrationYear(1934);

        HttpEntity<Vehicle> entity = new HttpEntity<>(vehicle, new HttpHeaders());

        ResponseEntity<ErrorResponse> errorResponseResponseEntity = restTemplate
                .exchange(createURLWithPort("/insurance-fee?pi=false", port), HttpMethod.POST, entity,
                        ErrorResponse.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, errorResponseResponseEntity.getStatusCode());
        Assert.assertEquals("Validation Failed!",
                Objects.requireNonNull(errorResponseResponseEntity.getBody()).getMessage());
        Assert.assertEquals("Invalid plate number!", errorResponseResponseEntity.getBody().getDetails().get(0));
    }
}
