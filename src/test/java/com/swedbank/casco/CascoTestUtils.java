package com.swedbank.casco;

/**
 * Utils for testing this application
 *
 * @author Vinod John
 */
public class CascoTestUtils {
    public static String createURLWithPort(String uri, int port) {
        return "http://localhost:" + port + uri;
    }
}
