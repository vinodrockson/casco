package com.swedbank.casco;

import com.swedbank.casco.dtos.SettingListDto;
import com.swedbank.casco.models.Setting;
import com.swedbank.casco.models.SettingType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;

import static com.swedbank.casco.CascoTestUtils.createURLWithPort;

/**
 * Tests for Setting Controller endpoints
 *
 * @author Vinod John
 */
@RunWith(SpringRunner.class)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SettingControllerTests {
    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void testGetSettingPageDefault() {
        ResponseEntity<SettingListDto> settingListDtoResponseEntity = restTemplate
                .getForEntity(createURLWithPort("/setting", port), SettingListDto.class);

        Assert.assertEquals(HttpStatus.OK, settingListDtoResponseEntity.getStatusCode());
        Assert.assertEquals(0,
                Objects.requireNonNull(settingListDtoResponseEntity.getBody()).getCurrentPage());
        Assert.assertEquals(3, settingListDtoResponseEntity.getBody().getTotalElements());
        Assert.assertEquals(3, settingListDtoResponseEntity.getBody().getSettingList().size());
    }

    @Test
    public void testGetSettingPageWithParam() {
        ResponseEntity<SettingListDto> settingListDtoResponseEntity = restTemplate
                .getForEntity(createURLWithPort("/setting?items=5", port),
                        SettingListDto.class);

        Assert.assertEquals(HttpStatus.OK, settingListDtoResponseEntity.getStatusCode());
        Assert.assertEquals(0,
                Objects.requireNonNull(settingListDtoResponseEntity.getBody()).getCurrentPage());
        Assert.assertEquals(3, settingListDtoResponseEntity.getBody().getTotalElements());
        Assert.assertEquals(3, settingListDtoResponseEntity.getBody().getSettingList().size());
    }

    @Test
    public void testCreateSetting() {
        Setting setting = new Setting();
        setting.setActive(true);
        setting.setName("MILEAGE");
        setting.setValue("1.45");
        setting.setSettingType(SettingType.RISK_COEFFICIENT);
        setting.setActive(true);

        HttpEntity<Setting> entity = new HttpEntity<>(setting, new HttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate
                .exchange(createURLWithPort("/setting", port), HttpMethod.POST, entity, String.class);

        Assert.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }
}
