package com.swedbank.casco;

import com.swedbank.casco.dtos.VehicleListDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.swedbank.casco.CascoTestUtils.createURLWithPort;
import static com.swedbank.casco.utils.Constants.Data.DEFAULT_ITEMS_PER_PAGE;

/**
 * Tests for Vehicle Controller endpoints
 *
 * @author Vinod John
 */
@RunWith(SpringRunner.class)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VehicleControllerTests {
    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void testGetVehiclesPageDefault() {
        ResponseEntity<VehicleListDto> vehicleListDtoResponseEntity = restTemplate
                .getForEntity(createURLWithPort("/vehicle", port), VehicleListDto.class);

        Assert.assertEquals(HttpStatus.OK, vehicleListDtoResponseEntity.getStatusCode());
        Assert.assertEquals(0, Objects.requireNonNull(vehicleListDtoResponseEntity.getBody()).getCurrentPage());
        Assert.assertEquals(64311, vehicleListDtoResponseEntity.getBody().getTotalElements());
        Assert.assertEquals(Integer.parseInt(DEFAULT_ITEMS_PER_PAGE),
                vehicleListDtoResponseEntity.getBody().getVehicleList().size());
    }

    @Test
    public void testGetVehiclesPageWithParam() {
        ResponseEntity<VehicleListDto> vehicleListDtoResponseEntity = restTemplate
                .getForEntity(createURLWithPort("/vehicle?page=5&items=100", port), VehicleListDto.class);

        Assert.assertEquals(HttpStatus.OK, vehicleListDtoResponseEntity.getStatusCode());
        Assert.assertEquals(5, Objects.requireNonNull(vehicleListDtoResponseEntity.getBody()).getCurrentPage());
        Assert.assertEquals(64311, vehicleListDtoResponseEntity.getBody().getTotalElements());
        Assert.assertEquals(100, vehicleListDtoResponseEntity.getBody().getVehicleList().size());
    }

    @Test
    public void testGetValidVehicleFields() {
        List<String> expectedList = Arrays.asList("FIRST_REGISTRATION_YEAR", "PURCHASE_PRICE", "MILEAGE",
                "PREVIOUS_INDEMNITY");
        ResponseEntity<String> vehicleFields = restTemplate
                .getForEntity(createURLWithPort("/vehicle/fields", port), String.class);
        List<String> actualList = Arrays.asList(Objects.requireNonNull(vehicleFields.getBody())
                .replaceAll("\\[", "").replaceAll("\\]", "")
                .replace("\"", "").split(","));

        Collections.sort(expectedList);
        Collections.sort(actualList);

        Assert.assertEquals(expectedList, actualList);
    }
}
