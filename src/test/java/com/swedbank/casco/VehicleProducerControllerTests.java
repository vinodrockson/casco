package com.swedbank.casco;

import com.swedbank.casco.dtos.VehicleProducerDto;
import com.swedbank.casco.dtos.VehicleProducerListDto;
import com.swedbank.casco.handlers.exception.ErrorResponse;
import com.swedbank.casco.models.VehicleProducer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Objects;

import static com.swedbank.casco.CascoTestUtils.createURLWithPort;
import static com.swedbank.casco.utils.Constants.Data.DEFAULT_ITEMS_PER_PAGE;

/**
 * Tests for Vehicle Producer Controller endpoints
 *
 * @author Vinod John
 */
@RunWith(SpringRunner.class)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VehicleProducerControllerTests {
    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void testGetVehicleProducerPageDefault() {
        ResponseEntity<VehicleProducerListDto> vehicleProducerListDtoResponseEntity = restTemplate
                .getForEntity(createURLWithPort("/vehicle-producer", port), VehicleProducerListDto.class);

        Assert.assertEquals(HttpStatus.OK, vehicleProducerListDtoResponseEntity.getStatusCode());
        Assert.assertEquals(0,
                Objects.requireNonNull(vehicleProducerListDtoResponseEntity.getBody()).getCurrentPage());
        Assert.assertEquals(14, vehicleProducerListDtoResponseEntity.getBody().getTotalElements());
        Assert.assertEquals(Integer.parseInt(DEFAULT_ITEMS_PER_PAGE),
                vehicleProducerListDtoResponseEntity.getBody().getVehicleProducerList().size());
    }

    @Test
    public void testGetVehicleProducerPageWithParam() {
        ResponseEntity<VehicleProducerListDto> vehicleProducerListDtoResponseEntity = restTemplate
                .getForEntity(createURLWithPort("/vehicle-producer?page=1&items=5", port),
                        VehicleProducerListDto.class);

        Assert.assertEquals(HttpStatus.OK, vehicleProducerListDtoResponseEntity.getStatusCode());
        Assert.assertEquals(1,
                Objects.requireNonNull(vehicleProducerListDtoResponseEntity.getBody()).getCurrentPage());
        Assert.assertEquals(14, vehicleProducerListDtoResponseEntity.getBody().getTotalElements());
        Assert.assertEquals(5, vehicleProducerListDtoResponseEntity.getBody().getVehicleProducerList().size());
    }

    @Test
    public void testGetActiveVehicleProducer() {
        ResponseEntity<VehicleProducerDto[]> vehicleProducerDtoEntity = restTemplate
                .getForEntity(createURLWithPort("/vehicle-producer/active", port), VehicleProducerDto[].class);

        Assert.assertEquals(HttpStatus.OK, vehicleProducerDtoEntity.getStatusCode());
        Assert.assertEquals(11, Objects.requireNonNull(vehicleProducerDtoEntity.getBody()).length);
    }

    @Test
    public void testCreateVehicleProducer() {
        VehicleProducer vehicleProducer = new VehicleProducer();
        vehicleProducer.setActive(true);
        vehicleProducer.setName("Mahindra");
        vehicleProducer.setAveragePurchasePrice(BigDecimal.valueOf(35000));
        vehicleProducer.setRiskCoefficient(1.45);

        HttpEntity<VehicleProducer> entity = new HttpEntity<>(vehicleProducer, new HttpHeaders());
        ResponseEntity<String> responseEntity = restTemplate
                .exchange(createURLWithPort("/vehicle-producer", port), HttpMethod.POST, entity, String.class);

        Assert.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    public void testCreateVehicleProducerExists() {
        VehicleProducer vehicleProducer = new VehicleProducer();
        vehicleProducer.setActive(true);
        vehicleProducer.setName("Audi");
        vehicleProducer.setAveragePurchasePrice(BigDecimal.valueOf(65278));
        vehicleProducer.setRiskCoefficient(0.56);

        HttpEntity<VehicleProducer> entity = new HttpEntity<>(vehicleProducer, new HttpHeaders());
        ResponseEntity<ErrorResponse> errorResponseResponseEntity = restTemplate
                .exchange(createURLWithPort("/vehicle-producer", port), HttpMethod.POST, entity,
                        ErrorResponse.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, errorResponseResponseEntity.getStatusCode());
        Assert.assertEquals("Validation Failed!",
                Objects.requireNonNull(errorResponseResponseEntity.getBody()).getMessage());
        Assert.assertEquals("Car already exists!", errorResponseResponseEntity.getBody().getDetails().get(0));
    }

    @Test
    public void testUpdateVehicleProducerError() {
        HttpEntity<VehicleProducer> entity = new HttpEntity<>(new VehicleProducer(), new HttpHeaders());
        ResponseEntity<ErrorResponse> errorResponseResponseEntity = restTemplate
                .exchange(createURLWithPort("/vehicle-producer", port), HttpMethod.PUT, entity, ErrorResponse.class);

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, errorResponseResponseEntity.getStatusCode());
        Assert.assertEquals("Server Error!",
                Objects.requireNonNull(errorResponseResponseEntity.getBody()).getMessage());
    }
}
