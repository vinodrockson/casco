# Swedbank Casco Insurance Back-end application 

This project is a back-end application for ***Casco Insurance application*** based on ***Spring Boot, Maven and Java 11***.

Note: Lombok library has been used in this project. So, Lombok plugin may needed to run this project. In case of Lombok crash, re-enable and run again.

## Database
H2 database has been used as an in-memory database.

There is an option to view database by navigation to path `/h2-console`. The database can be found in `application.properties`

## API documentation
Swagger has been implemented in this app for API documentation.

For development environment, swagger can be accessed from [here](http://localhost:8080/swagger-ui/index.html#/).

For production environment, swagger can be accessed from [here](http://3.80.63.202:8080/swagger-ui/index.html#/).

## Test
The test cases for this application can be found in `src/test/java/com.swedbank.casco`.

## Before run
Before running this project, do check the java version and change if necessary in `pom.xml`

## Development environment

Run `maven clean install` and `mvn package` and open `http://localhost:8080/` in an internet browser. 

## Production environment

This application had already dockerized and deployed to Amazon Web Services(AWS) via [Bitbucket Pipeline](https://bitbucket.org/vinodrockson/casco/addon/pipelines/home).

Check this [link](http://3.80.63.202:8080) to go production server. 


## After run

After run has completed, run the front-end ui project based on the environment

Please refer to [Casco Client Documentation](https://bitbucket.org/vinodrockson/cascoclient) for more details.



